# laravel-orm

`illuminate/database` 适配 `herosphp`

## 安装

```shell
composer require herosphp/orm
```
## 生成默认配置文件

```php
composer vendor:publish "herosphp/orm"
```

## 使用
```shell
<?php
declare(strict_types=1);
namespace app\bootstrap;

use herosphp\plugin\orm;

/**
 * Laravel启动器
 */
class LaravelStarter extends LaravelDbStarter
{
    //自定义分页参数名称
    //protected static string $pageName = "page";
}

LaravelStarter::init();
```

